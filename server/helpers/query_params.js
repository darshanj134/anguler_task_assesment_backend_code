const Joi = require('joi');

const schema = Joi.object().keys({
    utility_board_id: Joi.number().required(),
    sub_division_id: Joi.number(),
    agency_id: Joi.number(),
    consumer_account_number: Joi.number()
})
module.exports = schema;