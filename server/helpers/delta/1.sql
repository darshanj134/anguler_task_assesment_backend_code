ALTER TABLE address_details ADD door_no varchar(100) NULL;
ALTER TABLE address_details CHANGE door_no door_no varchar(100) NULL AFTER address_primary;

ALTER TABLE address_details ADD ward_no varchar(100) NULL;
ALTER TABLE address_details CHANGE ward_no ward_no varchar(100) NULL AFTER door_no;

ALTER TABLE address_details ADD grid_no varchar(100) NULL;
ALTER TABLE address_details CHANGE grid_no grid_no varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER buliding_number;

ALTER TABLE address_details ADD area_name varchar(100) NULL;
ALTER TABLE address_details CHANGE area_name area_name varchar(100) NULL AFTER grid_no;

ALTER TABLE address_details ADD street_name varchar(100) NULL;
ALTER TABLE address_details CHANGE street_name street_name varchar(100) NULL AFTER road_name;

ALTER TABLE address_details ADD building_type varchar(100) NULL;
ALTER TABLE address_details CHANGE building_type building_type varchar(100) NULL AFTER road_name;

ALTER TABLE address_details ADD number_of_floors varchar(100) NULL;
ALTER TABLE address_details CHANGE number_of_floors number_of_floors varchar(100) NULL AFTER buliding_number;

ALTER TABLE address_details ADD flore_no varchar(100) NULL;
ALTER TABLE address_details CHANGE flore_no flore_no varchar(100) NULL AFTER number_of_floors;
ALTER TABLE address_details CHANGE number_of_floors number_of_floors varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER building_name;


ALTER TABLE address_details ADD no_of_person_in_house varchar(100) NULL;
ALTER TABLE address_details CHANGE no_of_person_in_house no_of_person_in_house varchar(100) NULL AFTER flore_no;
