var mysql = require('mysql');
var db_config = require('../helpers/dbconfig.json');
const logger = require('./logging')
let db_name = "";
let db_hostname = "";
let db_password = "";
let db_username = "";

if (global.Env == 'dev') {
    db_name = db_config.dev.db_name;
    db_hostname = db_config.dev.db_hostname;
    db_password = db_config.dev.db_password;
    db_username = db_config.dev.db_username;
} else if (global.Env == 'local') {
    db_name = db_config.local.db_name;
    db_hostname = db_config.local.db_hostname;
    db_password = db_config.local.db_password;
    db_username = db_config.local.db_username;
} else if (global.Env == 'test') {
    db_name = db_config.test.db_name;
    db_hostname = db_config.test.db_hostname;
    db_password = db_config.test.db_password;
    db_username = db_config.test.db_username;
} else if (global.Env == 'production') {
    db_name = db_config.production.db_name;
    db_hostname = db_config.production.db_hostname;
    db_password = db_config.production.db_password;
    db_username = db_config.production.db_username;
}
var con = mysql.createPool({
    connectionLimit: 10,
    host: db_hostname,
    user: db_username,
    password: db_password,
    database: db_name
});
global.con = con;
var tcp_con = mysql.createPool({
    connectionLimit: 10,
    host: db_hostname,
    user: db_username,
    password: db_password,
    database: 'tcp_db'
});
global.tcp_con = tcp_con;
tcp_con.getConnection(function(err) {
    if (err) {
        console.info("Error in connecting with TCP database");
        return false;
    } else {
        return true;
        console.info("Database Connected!");
    }
});
con.getConnection(function(err) {
    if (err) {
        logger.error("Error in connecting with database");
        logger.error(err);
        throw err;
    } else {
        logger.info("Database Connected!");
        var firstQuery = "set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'";
        var secondQuery = "set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'";
        con.query(firstQuery, function(err, result) {
            if (err) {
                logger.error("Error in connecting with database");
                logger.error(err);
                throw err;
            } else {
                logger.info(firstQuery);
            }
        });
        con.query(secondQuery, function(err, result) {
            if (err) {
                logger.error("Error in connecting with database");
                logger.error(err);
                throw err;
            } else {
                logger.info(secondQuery);
            }
        });
    }
});

module.exports = con;