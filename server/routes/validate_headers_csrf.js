const Joi = require('joi');

const schema = Joi.object().keys({
    dsmart_authorization: Joi.string().required()
}).unknown(true);
module.exports = schema;