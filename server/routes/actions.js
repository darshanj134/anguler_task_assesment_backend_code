const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../helpers/jwt.json');
const Joi = require('joi');
let schema = require('../helpers/schema.js')
let verifyToken = require('../config/auth')
let sendResponse = require('../helpers/sendResponse');
let actions_validation = require('../actions/schema/actions_schema.js');
router
    .post(['/create'], verifyToken, (req, res) => {
        const { error } = Joi.validate(req.body, actions_validation);
        if (error) {
            const response = {
                success: false,
                type: 'response',
                message: error.details[0].message
            }
            sendResponse(req, res, response);
        } else {
            require('../actions/api/create.js')(req, res, sendResponse);
        }

    })
    .get(['/list'], verifyToken, (req, res) => {
        require('../actions/api/list.js')(req, res, sendResponse);
    })
    .put(['/update'], verifyToken, (req, res) => {
        const { error } = Joi.validate(req.body, actions_validation);
        if (error) {
            const response = {
                success: false,
                type: 'response',
                message: error.details[0].message
            }
            sendResponse(req, res, response);
        } else {
            require('../actions/api/update.js')(req, res, sendResponse);
        }
    })
module.exports = router;