const Joi = require('joi');

const schema = Joi.object().keys({
    'x-access-token': Joi.string().required()
}).unknown(true);
module.exports = schema;