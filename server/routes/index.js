const bodyParser = require('body-parser');
let csrf_validation_flag = false;
let auth_token_flag = false;
let load_routes = false;
const Joi = require('joi');
let validate_header_csrf = require('./validate_headers_csrf.js')
let validate_header_access_toke = require('./validate_header_access.js');
let validate_header_content_type = require('./validate_header_cont_type.js');
let sendResponse = require('../helpers/sendResponse');
const home = require('./home.js');
const users = require('./users.js');
const login = require('./login.js');

module.exports = routes;

function routes(app) {
    app.use(bodyParser.json());
    validate_header_csrf
    app.use(function(req, res, next) {
        logger.info(" Cors ");
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Origin,X-Requested-With,Accept,Content-Type,Authorization');
        res.header('Access-Control-Allow-Credentials', 'true');
        next();
    });
    app.use(function(req, res, next) {
        logger.info(req['originalUrl']);
        const { error } = Joi.validate(req.headers, validate_header_csrf);
        if (error) {
            const response = {
                success: false,
                type: 'request_for_csrf'
            }
            sendResponse(req, res, response);
        } else {
            if (req['originalUrl'] == '/validate/login') {
                app.use('/validate', login);
            } else {
                const { error } = Joi.validate(req.headers, validate_header_access_toke);
                if (error) {
                    const response = {
                        success: false,
                        type: 'request_for_csrf'
                    }
                    sendResponse(req, res, response);
                } else {
                    console.log("ok");
                    csrf_validation_flag = require('../config/csrf_auth.js')(req.headers);
                    logger.info(csrf_validation_flag);
                    if (csrf_validation_flag == true) {
                        global.validate_api = true;
                        app.use('/me', home);
                        app.use('/users', users);
                    } else {
                        res.status(401).json({
                            status: 'Unauthorized',
                        });
                    }
                }
            }
            next();
        }
    });
}