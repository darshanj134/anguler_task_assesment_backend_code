const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../helpers/jwt.json');
const Joi = require('joi');
let schema = require('../helpers/schema.js')
let sendResponse = require('../helpers/sendResponse');
let register_validation = require('../users/schema/users_create_schema.js');
const user_address = require('../users/schema/address_schema.js');
router
    .post(['/create'], (req, res) => {
        const { error } = Joi.validate(req.body, register_validation);
        if (error) {
            const response = {
                success: false,
                type: 'response',
                message: error.details[0].message
            }
            sendResponse(req, res, response);
        } else {
            require('../users/api/register.js')(req, res, sendResponse);
        }
    })
    .get(['/list'], (req, res) => {
        require('../users/api/list.js')(req, res, sendResponse);
    })
    .get(['/repo/list'], (req, res) => {
        require('../users/api/get_repo_names.js')(req, res, sendResponse);
    })
    .post(['/add/address'], (req, res) => {
        const { error } = Joi.validate(req.body, user_address);
        if (error) {
            const response = {
                success: false,
                type: 'response',
                message: error.details[0].message
            }
            sendResponse(req, res, response);
        } else {
            require('../users/api/add_address.js')(req, res, sendResponse);
        }
    })
    .get(['/address/list'], (req, res) => {
        require('../users/api/lis_address.js')(req, res, sendResponse);
    })
module.exports = router;