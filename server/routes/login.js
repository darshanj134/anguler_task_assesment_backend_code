const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../helpers/jwt.json');
const Joi = require('joi');
let schema = require('../helpers/schema.js')
let sendResponse = require('../helpers/sendResponse');
let login_validation = require('../logins/schema/login_schema.js');
router
    .post(['/login'], (req, res) => {
        const { error } = Joi.validate(req.body, login_validation);
        if (error) {
            const response = {
                success: false,
                type: 'response',
                message: error.details[0].message
            }
            sendResponse(req, res, response);
        } else {
            require('../logins/api/login.js')(req, res, sendResponse);
        }
    })
module.exports = router;