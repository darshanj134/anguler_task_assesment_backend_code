const Joi = require('joi');

const schema = Joi.object().keys({
    'content-type': Joi.string().required()
}).unknown(true);
module.exports = schema;