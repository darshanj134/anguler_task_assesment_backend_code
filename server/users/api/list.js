'use strict';
module.exports = getAll;

function getAll(req, res, sendResponse) {
    let data = {};
    let message = "";
    let success = "";
    let sql = "";
    if (req.query.id == "all") {
        sql = "select * from users";
    } else {
        sql = "select * from users where id='" + req.query.id + "'";
    }
    con.query(sql, function(err, result) {
        if (err) {
            let response = {
                type: "db_error",
                error: err
            }
            sendResponse(req, res, response);
        } else {
            if (result.length > 0) {
                success = true;
                data = result;
                message = "got data"
            } else {
                success = false;
                data = {};
                message = "No Records Found !";
            }
            const response = {
                type: "response",
                success: success,
                data: data,
                message: message
            }
            sendResponse(req, res, response);
        }
    });

}