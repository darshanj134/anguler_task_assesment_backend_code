'use strict'
module.exports = create;

function create(req, res, sendResponse) {
    var today = new Date();
    let success = "";
    let message = "";
    let data = {}
    let sql = "";
    let addressData = {
        "object_id": req.body.object_id,
        "address_line_1": req.body.address_line_1,
        "address_line_2": req.body.address_line_2,
        "building_name": req.body.building_name,
        "area_name": req.body.area_name,
        "road_name": req.body.road_name,
        "country": req.body.country,
        "state": req.body.state,
        "city": req.body.city,
        "pincode": req.body.pincode,
        "land_mark": req.body.land_mark,
        "created_by": req.body.created_by,
        "created_on": today
    }
    sql = "INSERT INTO address_details SET ?"
    con.query(sql, addressData, function(error, result) {
        if (error) {
            console.info(error);
            let response = {
                success: false,
                type: 'response',
                data: error,
                message: "db_error"
            }
            sendResponse(req, res, response);
        } else {
            success = true;
            message = "Address Created Successfully";
            data = result
            const response = {
                success: success,
                message: message,
                data: data,
                type: 'response'
            }
            sendResponse(req, res, response);
        }
    })
}