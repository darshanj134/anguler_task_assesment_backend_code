'use strict'
module.exports = create;
var sha1 = require('sha1');

function create(req, res, sendResponse) {
    var today = new Date();
    let success = "";
    let message = "";
    let data = {}
    let sql = "";
    let pwd = sha1(req.body.password);
    let registerData = {
        "salutation": req.body.salutation,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email_id": req.body.email_id,
        "mobile_number": req.body.mobile_number,
        "age": req.body.age,
        "gender": req.body.gender,
        "password": pwd,
        "created_on": today
    }
    sql = "INSERT INTO users SET ?"
    con.query(sql, registerData, function(error, result) {
        if (error) {
            console.info(error);
            let response = {
                success: false,
                type: 'response',
                data: error,
                message: "db_error"
            }
            sendResponse(req, res, response);
        } else {
            success = true;
            message = "User Created Successfully";
            data = result
            const response = {
                success: success,
                message: message,
                data: data,
                type: 'response'
            }
            sendResponse(req, res, response);
        }
    })
}