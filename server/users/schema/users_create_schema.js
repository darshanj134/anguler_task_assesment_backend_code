const Joi = require('joi');

const schema = Joi.object().keys({
    email_id: Joi.string(),
    salutation: Joi.string().required(),
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
    mobile_number: Joi.number().required(),
    age: Joi.number(),
    gender: Joi,
    password: Joi.string().required(),
    created_by: Joi,
    updated_by: Joi
})
module.exports = schema;