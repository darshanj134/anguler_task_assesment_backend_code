const Joi = require('joi');

const schema = Joi.object().keys({
    object_id: Joi,
    address_line_1: Joi,
    address_line_2: Joi,
    building_name: Joi,
    area_name: Joi,
    road_name: Joi,
    country: Joi,
    state: Joi,
    city: Joi,
    pincode: Joi,
    land_mark: Joi,
    created_by: Joi
})
module.exports = schema;