'use strict';
module.exports = get;
const jwt = require('jsonwebtoken');
const config = require('../../helpers/jwt.json');
var sha1 = require('sha1');

function get(req, res, sendResponse) {
    console.info(req.body);
    let data = {};
    let message = "";
    let success = "";
    let sql = "";
    let pwd = sha1(req.body.password);
    let email = req.body.email_id;
    sql = "select * from users where email_id = '" + email + "' AND password = '" + pwd + "'";
    con.query(sql,
        function(err, result) {
            if (err) {
                logger.info(err);
                let response = {
                    type: "db_error",
                    error: err
                }
                sendResponse(req, res, response);
            } else {
                if (result.length >= 1) {
                    let user_details = {
                        id: result[0].user_id,
                        user_name: result[0].first_name,
                        email_id: result[0].email_id
                    };
                    var token = jwt.sign(user_details, config.secret_key, {
                        expiresIn: '14d'
                    });
                    success = true;
                    token = token;
                    data = result;
                    message = "Login Success";
                } else {
                    success = false;
                    data = {};
                    message = "Please Enter Valid Login Credentials";
                }
                const response = {
                    type: "response",
                    success: success,
                    token: token,
                    data: data,
                    message: message
                };
                sendResponse(req, res, response);
            }
        });
}