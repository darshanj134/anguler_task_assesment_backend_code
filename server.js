const express = require('express');
const helmet = require('helmet');
const app = express();
const port = process.env.PORT || 1000;
global.Env = process.env.NODE_ENV || 'local'
const logger = require('./server/config/logging')
logger.info("<---------------------------------------------------------------------------------------------------------------------------->");
// app.use(helmet.xssFilter());
// app.use(helmet.frameguard({
//     action: 'sameorigin'
// }));
// if (global.Env != 'production') {
//     require('babel-core/register');
//     require('babel-polyfill');
// }
logger.info("<---------------------------------------------------------------------------------------------------------------------------->");
require('./server/config/database')
require('./server/routes/index')(app);
app.listen(port, () => logger.info(`Server running on port ${port}`));
module.exports = app;